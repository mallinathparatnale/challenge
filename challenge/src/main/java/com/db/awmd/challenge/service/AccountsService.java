package com.db.awmd.challenge.service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.repository.AccountsRepository;
import lombok.Getter;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {

  @Getter
  private final AccountsRepository accountsRepository;

  @Autowired
  public AccountsService(AccountsRepository accountsRepository) {
    this.accountsRepository = accountsRepository;
  }

  public void createAccount(Account account) {
    this.accountsRepository.createAccount(account);
  }

  public Account getAccount(String accountId) {
    return this.accountsRepository.getAccount(accountId);
  }
  public void transferMoney(Account fromAccount,Account toAccount, BigDecimal balance) {
	     this.accountsRepository.transferMoney( fromAccount,toAccount,balance);
	  }

public void withdraw(Account account, BigDecimal transferAmt) {
	this.accountsRepository.withdraw( account,transferAmt);
}

public void deposit(Account account, BigDecimal transferAmt) {
	this.accountsRepository.deposit( account,transferAmt);
}
}
