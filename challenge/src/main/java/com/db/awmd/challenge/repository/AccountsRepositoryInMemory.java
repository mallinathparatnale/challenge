package com.db.awmd.challenge.repository;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.exception.LowAccountBalanceException;
import com.db.awmd.challenge.service.EmailNotificationService;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountsRepositoryInMemory implements AccountsRepository {

  private final Map<String, Account> accounts = new ConcurrentHashMap<>();
  private final EmailNotificationService emailNotificationService;
  
  @Autowired
  public AccountsRepositoryInMemory(EmailNotificationService emailNotificationService) {
    this.emailNotificationService = emailNotificationService;
  }
  @Override
  public void createAccount(Account account) throws DuplicateAccountIdException {
    Account previousAccount = accounts.putIfAbsent(account.getAccountId(), account);
    if (previousAccount != null) {
      throw new DuplicateAccountIdException(
        "Account id " + account.getAccountId() + " already exists!");
    }
  }

  @Override
  public Account getAccount(String accountId) {
    return accounts.get(accountId);
  }
  
  @Override
  public void withdraw(Account fromAcc, BigDecimal balance) throws LowAccountBalanceException{
	  fromAcc = getAccount(fromAcc.getAccountId());
	 BigDecimal  bal = fromAcc.getBalance();
	 System.out.println(bal);
	 BigDecimal remainingBalance = bal.subtract(balance);
	 if(remainingBalance.signum()>= 0) {
		 fromAcc.setBalance(remainingBalance);
	 }else {
		 throw new LowAccountBalanceException(
			        "Don't have sufficient balance to transfer in Account " + fromAcc.getAccountId());
	 }
  }
  @Override
  public void deposit(Account toAcc, BigDecimal balance) {
	        toAcc = getAccount(toAcc.getAccountId());
		 BigDecimal  bal = toAcc.getBalance();
		 BigDecimal totalBal = bal.add(balance);
		 toAcc.setBalance(totalBal);
	  }
  
  @Override
  public void transferMoney(Account fromAccount,Account toAccount, BigDecimal balance) {
	  synchronized(fromAccount) {
		  withdraw(fromAccount,balance);
		     synchronized(toAccount) {
			     deposit(toAccount,balance);
		     }
	  }
	  emailNotificationService.notifyAboutTransfer(fromAccount,"Rs."+balance+" debited from your account "+fromAccount.getAccountId());
	  emailNotificationService.notifyAboutTransfer(toAccount,"Your account "+fromAccount.getAccountId()+" is created by Rs. "+balance);
  }

  @Override
  public void clearAccounts() {
    accounts.clear();
  }

}
