package com.db.awmd.challenge.exception;

public class LowAccountBalanceException extends RuntimeException {

	  public LowAccountBalanceException(String message) {
	    super(message);
	  }
}
