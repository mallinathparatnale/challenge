package com.db.awmd.challenge.repository;

import java.math.BigDecimal;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;

public interface AccountsRepository {

  void createAccount(Account account) throws DuplicateAccountIdException;

  Account getAccount(String accountId);
   void  transferMoney(Account fromAccount,Account toAccount, BigDecimal balance);
   void withdraw(Account fromAcc, BigDecimal balance);
   void deposit(Account toAcc, BigDecimal balance);
  void clearAccounts();
}
