package com.db.awmd.challenge;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.exception.LowAccountBalanceException;
import com.db.awmd.challenge.service.AccountsService;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsServiceTest {

  @Autowired
  private AccountsService accountsService;

  @Test
  public void addAccount() throws Exception {
    Account account = new Account("Id-123");
    account.setBalance(new BigDecimal(1000));
    this.accountsService.createAccount(account);

    assertThat(this.accountsService.getAccount("Id-123")).isEqualTo(account);
  }

  @Test
  public void addAccount_failsOnDuplicateId() throws Exception {
    String uniqueId = "Id-" + System.currentTimeMillis();
    Account account = new Account(uniqueId);
    this.accountsService.createAccount(account);

    try {
      this.accountsService.createAccount(account);
      fail("Should have failed when adding duplicate account");
    } catch (DuplicateAccountIdException ex) {
      assertThat(ex.getMessage()).isEqualTo("Account id " + uniqueId + " already exists!");
    }

  }
  
  @Test
  public void transferMoneyWithNegativeBalance() throws Exception {
	  String uniqueId = "Id-" + Math.random();
	   Account account = new Account(uniqueId, new BigDecimal("100"));
	    this.accountsService.createAccount(account);
	    String uniqueId2 = "Id-" + Math.random();
	    Account account2 = new Account(uniqueId2, new BigDecimal("50"));
	    this.accountsService.createAccount(account2);
	    BigDecimal transferAmt=  new BigDecimal("300");
    try {
      this.accountsService.transferMoney(account, account2,transferAmt);
      fail("Should have failed when transfering negative balance "+account.getAccountId());
    } catch (LowAccountBalanceException ex) {
      assertThat(ex.getMessage()).isEqualTo("Don't have sufficient balance to transfer in Account "+account.getAccountId());
    }

  }
  
  @Test
  public void transferMoneyTest() throws Exception {
	  String uniqueId = "Id-"  + Math.random();
	   Account account = new Account(uniqueId, new BigDecimal("100"));
	    this.accountsService.createAccount(account);
	    String uniqueId2 = "Id-" + Math.random();
	    Account account2 = new Account(uniqueId2, new BigDecimal("50"));
	    this.accountsService.createAccount(account2);
	    BigDecimal transferAmt=  new BigDecimal("100");
      this.accountsService.transferMoney(account, account2,transferAmt);
      assertThat(account.getBalance()).isEqualTo("0");
      assertThat(account2.getBalance()).isEqualTo("150");

  }
  
  @Test
  public void withdrawTestFailsOnNegativeBalance() throws Exception {
	  String uniqueId = "Id-" + Math.random();
	   Account account = new Account(uniqueId, new BigDecimal("100"));
	    this.accountsService.createAccount(account);
	    BigDecimal transferAmt=  new BigDecimal("300");
	   
   try {
	   this.accountsService.withdraw(account,transferAmt);
     fail("Should have failed when withdrawing negative balance ");
   } catch (LowAccountBalanceException ex) {
     assertThat(ex.getMessage()).isEqualTo("Don't have sufficient balance to transfer in Account " + account.getAccountId());
   }
  }
  @Test
  public void withdrawTest() throws Exception {
	  String uniqueId = "Id-" + Math.random();
	   Account account = new Account(uniqueId, new BigDecimal("100"));
	    this.accountsService.createAccount(account);
	    BigDecimal transferAmt=  new BigDecimal("100");
		   this.accountsService.withdraw(account,transferAmt);
	    assertThat(account.getBalance()).isEqualTo("0");
  }
  
  @Test
  public void depositTest() throws Exception {
	  String uniqueId = "Id-" + Math.random();
	   Account account = new Account(uniqueId, new BigDecimal("100"));
	    this.accountsService.createAccount(account);
	    BigDecimal transferAmt =  new BigDecimal("100");
		   this.accountsService.deposit(account,transferAmt);
	    assertThat(account.getBalance()).isEqualTo("200");
  }
  
}
